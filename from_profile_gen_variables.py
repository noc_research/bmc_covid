"""Generate clinical variables from patient profiles."""

import numpy as np
import csv
import re
import os

csv.field_size_limit(1000000000)
import copy
from datetime import datetime


def read_head(data):
    head = data[0]
    head_num_dic = {}
    for i in range(len(head)):
        head_num_dic[head[i]] = i
    return head_num_dic


folder = "profile_v0.11/"
filename_list = sorted(os.listdir(folder))


feature_list = [
    "covid_lab_test",
    "adm_date",
    "icu_date",
    "intubation_date",
    "death_date",
    "age",
    "gender",
    "race",
    "language",
    "zip",
    "BMI (Calculated)",
    "BP",
    "Height",
    "Pulse",
    "Resp",
    "SpO2",
    "Temp",
    "Weight",
    "immune",
    "symptom",
    "past medical history",
    "medication",
    "radiology findings",
    "other disease or symptoms",
    "lab result",
    "travel",
    "PHQ",
]


empty_feature_dic = {i: [] for i in feature_list}
all_patient_dic = {}

print(empty_feature_dic)


for filename in filename_list:
    PID = filename[:-4]
    print(PID)

    if PID not in all_patient_dic:
        all_patient_dic[PID] = copy.deepcopy(empty_feature_dic)

    data = [item for item in csv.reader(open(folder + filename, "r", encoding="utf-8"))]

    for line in data:
        feature = line[0]
        all_patient_dic[PID][feature].append(line[2:])


# ---------- Load over


def format_time(date):
    pass


def A_later_than_B(
    A="05/12/2020 14:34:00",
    B="05/12/2020",
    C=9999,
    relaxation=0,
    format_pattern="%m/%d/%Y %H:%M:%S",
):
    """Date A is later than (Date B-relaxation), and A later than B within C hours"""

    if len(A.split()) < 2:
        timeA = A + " 00:00:00"
    else:
        timeA = A
    if len(B.split()) < 2:
        timeB = B + " 00:00:00"
    else:
        timeB = B

    gap_seconds = (
        datetime.strptime(timeA, format_pattern)
        - datetime.strptime(timeB, format_pattern)
    ).total_seconds()

    return gap_seconds >= -int(relaxation * 3600) and gap_seconds <= int(C * 3600)


print(A_later_than_B())


def A_later_than_B_before_C(
    A="05/12/2020 14:34:00",
    B="05/11/2020",
    C="05/14/2020",
    relaxation=0,
    format_pattern="%m/%d/%Y %H:%M:%S",
):

    if len(A.split()) < 2:
        timeA = A + " 00:00:00"
    else:
        timeA = A
    if len(B.split()) < 2:
        timeB = B + " 00:00:00"
    else:
        timeB = B
    if len(C.split()) < 2:
        timeC = C + " 00:00:00"
    else:
        timeC = C

    gap_seconds_AB = (
        datetime.strptime(timeA, format_pattern)
        - datetime.strptime(timeB, format_pattern)
    ).total_seconds()
    gap_seconds_AC = (
        datetime.strptime(timeA, format_pattern)
        - datetime.strptime(timeC, format_pattern)
    ).total_seconds()

    return gap_seconds_AB >= -int(relaxation * 3600) and gap_seconds_AC <= 0


print(A_later_than_B_before_C())


def check_sym(words_list):
    sym_dic = {}
    for w in words_list:
        if w.lower().find("fever") != -1:
            sym_dic["sym_Fever"] = 1
        if w.lower().find("cough") != -1:
            sym_dic["sym_Cough"] = 1
        if (
            w.lower().find("dyspnea") != -1
            or w.lower().find("shortness of breath") != -1
            or w.find("SOB") != -1
        ):
            sym_dic["sym_Dyspnea"] = 1
        if w.lower().find("fatigue") != -1:
            sym_dic["sym_Fatigue"] = 1
        if w.lower().find("diarrhea") != -1 or w.lower().find("loose stool") != -1:
            sym_dic["sym_Diarrhea"] = 1
        if w.lower().find("nausea") != -1 or w.find("N/V") != -1:
            sym_dic["sym_Nausea"] = 1
        if (
            w.lower().find("vomiting") != -1
            or w.lower().find("vomit") != -1
            or w.lower().find("emesis") != -1
            or w.find("N/V") != -1
        ):
            sym_dic["sym_Vomiting"] = 1
        if w.lower().find("abdominal pain") != -1 or w.lower().find("abd pain") != -1:
            sym_dic["sym_Abdominal_pain"] = 1
        if (
            w.lower().find("loss of smell") != -1
            or w.lower().find("anosmia") != -1
            or w.lower().find("lack of smell") != -1
        ):
            sym_dic["sym_Loss_of_smell"] = 1
        if (
            w.lower().find("loss of taste") != -1
            or w.lower().find("lack of taste") != -1
        ):
            sym_dic["sym_Loss_of_taste"] = 1
        if w.lower().find("chest pain") != -1:
            sym_dic["sym_Chest_pain"] = 1
        if w.lower().find("headache") != -1:
            sym_dic["sym_Headache"] = 1
        if w.lower().find("sore throat") != -1:
            sym_dic["sym_Sore_throat"] = 1
        if w.lower().find("hemoptysis") != -1 or w.lower().find("bloody sputum") != -1:
            sym_dic["sym_Hemoptysis"] = 1
        if (
            w.lower().find("myalgia") != -1
            or w.lower().find("muscle ache") != -1
            or w.lower().find("muscleache") != -1
            or w.lower().find("muscle pain") != -1
        ):
            sym_dic["sym_Myalgia"] = 1

    return sym_dic


print(check_sym(["Fever", "dry cough"]))


def check_pmh(words_list):
    pmh_dic = {}
    for w in words_list:
        if (
            w.lower().find("hypertensive disorder") != -1
            or w.lower().find("hypertension") != -1
            or w.find("HTN") != -1
            or w.lower().find("high blood pressure") != -1
            or w.lower().find("elevated blood pressure") != -1
        ):
            pmh_dic["pmh_Hypertension"] = 1
        if (
            w.find("COPD") != -1
            or w.lower().find("chronic obstructive pulmonary disease") != -1
            or w.lower().find("emphysema") != -1
            or w.lower().find("chronic bronchitis") != -1
            or w.lower().find("chr bronchitis") != -1
        ):
            pmh_dic["pmh_COPD"] = 1
        if (
            w.lower().find("diabetes") != -1
            or w.lower().find("diabetic") != -1
            or w.find("DM type 2") != -1
            or w.find("DM type 1") != -1
            or w.find("DM2") != -1
            or w.find("DM1") != -1
        ):
            pmh_dic["pmh_Diabetes"] = 1
        if (
            w.find("CKD") != -1
            or w.lower().find("chronic kidney disease") != -1
            or w.lower().find("chronic renal failure") != -1
            or w.find("ESRD") != -1
            or w.lower().find("end-stage renal disease") != -1
            or w.find("CRF") != -1
            or w.lower().find("dialysis-dependent") != -1
        ):
            pmh_dic["pmh_CKD"] = 1
        if (
            w.find("CAD") != -1
            or w.lower().find("coronary artery disease") != -1
            or w.lower().find("angina") != -1
        ):
            pmh_dic["pmh_CAD"] = 1
        if (
            w.find("STEMI") != -1
            or w.find("NSTEMI") != -1
            or w.lower().find("myocardial infarction") != -1
        ):
            pmh_dic["pmh_MI"] = 1
        if w.lower().find("asthma") != -1:
            pmh_dic["pmh_Asthma"] = 1
        if w.lower().find("osteoarthritis") != -1 or w.lower() == "arthritis":
            pmh_dic["pmh_Osteoarthritis_arthritis"] = 1
        if w.find("SLE") != -1 or w.lower().find("lupus") != -1:
            pmh_dic["pmh_SLE"] = 1
        if (
            w.find("HLD") != -1
            or w.lower().find("hyperlipidemia") != -1
            or w.lower().find("hyperlipidaemia") != -1
            or w.lower().find("hypercholesterolemia") != -1
            or w.lower().find("high cholesterol") != -1
            or w.lower().find("elevated cholesterol") != -1
        ):
            pmh_dic["pmh_HLD"] = 1
        if (
            w.lower().find("arrhythmia") != -1
            or w.lower().find("atrial fibrillation") != -1
            or w.lower().find("ventricular tachycardia") != -1
            or w.lower().find("supraventricular tachycardia") != -1
            or w.lower().find("premature ventricular contraction") != -1
            or w.lower().find("atrioventricular block") != -1
            or w.lower().find("atrial flutter") != -1
        ):
            pmh_dic["pmh_Arrhythmia"] = 1
        if (
            w.lower().find("thyroid disease") != -1
            or w.lower().find("hyperthyroidism") != -1
            or w.lower().find("hypothyroidism") != -1
            or w.find("Graves") != -1
            or w.find("Hashimoto") != -1
            or w.lower().find("myxedema") != -1
            or w.lower().find("thyroiditis") != -1
        ):
            pmh_dic["pmh_Thyroid disease"] = 1
        if (
            w.lower().find("stroke") != -1
            or w.find("CVA") != -1
            or w.lower().find("cerebrovascular accident") != -1
            or w.lower().find("transient ischemic attack") != -1
            or w.find("TIA") != -1
        ):
            pmh_dic["pmh_Stroke"] = 1
        if w.lower().find("migraine") != -1:
            pmh_dic["pmh_Migraine"] = 1
        if w.lower().find("epilepsy") != -1:
            pmh_dic["pmh_Epilepsy"] = 1
        if w.find("Alzheimer") != -1 or w.lower().find("dementia") != -1:
            pmh_dic["pmh_Alzheimer"] = 1
        if w.find("Parkinson") != -1:
            pmh_dic["pmh_Parkinson"] = 1
        if (
            w.lower().find("nephrolithiasis") != -1
            or w.lower().find("kidney stone") != -1
        ):
            pmh_dic["pmh_Nephrolithiasis"] = 1
        if w.find("Cushing") != -1:
            pmh_dic["pmh_Cushing"] = 1
        if w.lower().find("adrenal insufficiency") != -1 or w.find("Addison") != -1:
            pmh_dic["pmh_Adrenal Insufficiency"] = 1
        if (
            w.lower().find("diverticulosis") != -1
            or w.lower().find("diverticulitis") != -1
        ):
            pmh_dic["pmh_Diverticulosis"] = 1
        if w.find("GERD") != -1 or w.lower().find("reflux disease") != -1:
            pmh_dic["pmh_GERD"] = 1
        if w.find("IBS") != -1 or w.lower().find("irritable bowel syndrome") != -1:
            pmh_dic["pmh_IBS"] = 1
        if (
            w.find("IBD") != -1
            or w.lower().find("inflammatory bowel disease") != -1
            or w.find("Crohn") != -1
            or w.lower().find("ulcerative colitis") != -1
        ):
            pmh_dic["pmh_IBD"] = 1
        if w.lower().find("cholelithiasis") != -1 or w.lower().find("gallstones") != -1:
            pmh_dic["pmh_Cholelithiasis"] = 1
        if (
            w.lower().find("inguinal hernia") != -1
            or w.lower().find("umbilical hernia") != -1
            or w.lower().find("ventral hernia") != -1
        ):
            pmh_dic["pmh_Inguinal hernia"] = 1
        if w.lower().find("hepatitis") != -1:
            pmh_dic["pmh_Hepatitis"] = 1
        if w.lower().find("cirrhosis") != -1:
            pmh_dic["pmh_Cirrhosis"] = 1
        if (
            w.lower().find("valvular") != -1
            or w.lower().find("aortic stenosis") != -1
            or w.lower().find("aortic regurgitation") != -1
            or w.lower().find("mitral stenosis") != -1
            or w.lower().find("mitral regurgitation") != -1
            or w.lower().find("aortic valve stenosis") != -1
        ):
            pmh_dic["pmh_Valvular disease"] = 1
        if w.find("CHF") != -1 or w.lower().find("heart failure") != -1:
            pmh_dic["pmh_CHF"] = 1
        if (
            w.find("PAD") != -1
            or w.lower().find("peripheral arterial disease") != -1
            or w.lower().find("claudication") != -1
        ):
            pmh_dic["pmh_PAD"] = 1
        if w.lower().find("osteoporosis") != -1:
            pmh_dic["pmh_Osteoporosis"] = 1
        if (
            w.lower().find("cancer") != -1
            or w.lower().find("carcinoma") != -1
            or w.lower().find("leukemia") != -1
            or w.lower().find("lymphoma") != -1
            or w.lower().find("myeloma") != -1
            or w.lower().find("sarcoma") != -1
        ):
            pmh_dic["pmh_Cancer"] = 1
        if (
            w.lower().find("tuberculosis") != -1
            or w.find("TB") != -1
            or w.lower().find("positive ppd") != -1
        ) and w.lower().find("brain") == -1:
            pmh_dic["pmh_TB"] = 1
        if w.lower().find("cardiomyopathy") != -1:
            pmh_dic["pmh_Cardiomyopathy"] = 1
        if w.find("AAA") != -1 or w.lower().find("abdominal aortic aneurysm") != -1:
            pmh_dic["pmh_AAA"] = 1
        if (
            w.find("DVT") != -1
            or w.lower().find("deep venous thrombosis") != -1
            or w.lower().find("pulmonary embolism") != -1
            or w.lower().find("deep vein thrombosis") != -1
        ):
            pmh_dic["pmh_DVT"] = 1
        if w.find("vWD") != -1 or w.lower().find("von willebrand") != -1:
            pmh_dic["pmh_vWD"] = 1
        if (
            w.lower().find("anemia") != -1
            or w.lower().find("iron deficiency") != -1
            or w.lower().find("thalassemia") != -1
        ):
            pmh_dic["pmh_Anemia"] = 1
        if (
            w.lower().find("transplantation") != -1
            or w.lower().find("transplant") != -1
        ):
            pmh_dic["pmh_Transplantation"] = 1
        if (
            (
                w.find("HIV") != -1
                or w.find("AIDS") != -1
                or w.lower().find("human immunodeficiency virus") != -1
                or w.lower().find("acquired immunodeficiency syndrome") != -1
            )
            and w.lower().find("exposure") == -1
            and w.lower().find("risk") == -1
            and w.lower().find("counseling") == -1
        ):
            pmh_dic["pmh_HIV"] = 1

    return pmh_dic


print(check_pmh(["HIV exposure", "positive ppd,s"]))


DATA_BASE = {}

empty_variables_dic = {
    "postive": {"value": 0, "date": []},
    "adm": {"value": 0, "date": []},
    "icu": {"value": 0, "date": []},
    "intu": {"value": 0, "date": []},
    "death": {"value": 0, "date": []},
    "bmi": {"value": [], "date": []},
    "s_bp": {"value": [], "date": []},
    "d_bp": {"value": [], "date": []},
    "pulse": {"value": [], "date": []},
    "resp": {"value": [], "date": []},
    "spo2": {"value": [], "date": []},
    "temp": {"value": [], "date": []},
    "Opacity": {"value": 0, "date": []},
    "Atelectasis": {"value": 0, "date": []},
    "Consolidation": {"value": 0, "date": []},
    "Pleural Effusion": {"value": 0, "date": []},
    "Pneumothorax": {"value": 0, "date": []},
    "Nodule": {"value": 0, "date": []},
    "sym_Fever": {"value": 0, "date": []},
    "sym_Cough": {"value": 0, "date": []},
    "sym_Dyspnea": {"value": 0, "date": []},
    "sym_Fatigue": {"value": 0, "date": []},
    "sym_Diarrhea": {"value": 0, "date": []},
    "sym_Nausea": {"value": 0, "date": []},
    "sym_Vomiting": {"value": 0, "date": []},
    "sym_Abdominal_pain": {"value": 0, "date": []},
    "sym_Loss_of_smell": {"value": 0, "date": []},
    "sym_Loss_of_taste": {"value": 0, "date": []},
    "sym_Chest_pain": {"value": 0, "date": []},
    "sym_Headache": {"value": 0, "date": []},
    "sym_Sore_throat": {"value": 0, "date": []},
    "sym_Hemoptysis": {"value": 0, "date": []},
    "sym_Myalgia": {"value": 0, "date": []},
    "pmh_Hypertension": {"value": 0, "date": []},
    "pmh_COPD": {"value": 0, "date": []},
    "pmh_Diabetes": {"value": 0, "date": []},
    "pmh_CKD": {"value": 0, "date": []},
    "pmh_CAD": {"value": 0, "date": []},
    "pmh_MI": {"value": 0, "date": []},
    "pmh_Asthma": {"value": 0, "date": []},
    "pmh_Osteoarthritis_arthritis": {"value": 0, "date": []},
    "pmh_SLE": {"value": 0, "date": []},
    "pmh_HLD": {"value": 0, "date": []},
    "pmh_Arrhythmia": {"value": 0, "date": []},
    "pmh_Thyroid disease": {"value": 0, "date": []},
    "pmh_Stroke": {"value": 0, "date": []},
    "pmh_Migraine": {"value": 0, "date": []},
    "pmh_Epilepsy": {"value": 0, "date": []},
    "pmh_Alzheimer": {"value": 0, "date": []},
    "pmh_Parkinson": {"value": 0, "date": []},
    "pmh_Nephrolithiasis": {"value": 0, "date": []},
    "pmh_Cushing": {"value": 0, "date": []},
    "pmh_Adrenal Insufficiency": {"value": 0, "date": []},
    "pmh_Diverticulosis": {"value": 0, "date": []},
    "pmh_GERD": {"value": 0, "date": []},
    "pmh_IBS": {"value": 0, "date": []},
    "pmh_IBD": {"value": 0, "date": []},
    "pmh_Cholelithiasis": {"value": 0, "date": []},
    "pmh_Inguinal hernia": {"value": 0, "date": []},
    "pmh_Hepatitis": {"value": 0, "date": []},
    "pmh_Cirrhosis": {"value": 0, "date": []},
    "pmh_Valvular disease": {"value": 0, "date": []},
    "pmh_CHF": {"value": 0, "date": []},
    "pmh_PAD": {"value": 0, "date": []},
    "pmh_Osteoporosis": {"value": 0, "date": []},
    "pmh_Cancer": {"value": 0, "date": []},
    "pmh_TB": {"value": 0, "date": []},
    "pmh_Cardiomyopathy": {"value": 0, "date": []},
    "pmh_AAA": {"value": 0, "date": []},
    "pmh_DVT": {"value": 0, "date": []},
    "pmh_vWD": {"value": 0, "date": []},
    "pmh_Anemia": {"value": 0, "date": []},
    "pmh_Transplantation": {"value": 0, "date": []},
    "pmh_HIV": {"value": 0, "date": []},
}


for PID in all_patient_dic:
    feature_dic = all_patient_dic[PID]

    DATA_BASE[PID] = copy.deepcopy(empty_variables_dic)

    # ------------------- Labels

    for f in feature_dic["covid_lab_test"]:
        if f[1].lower() == "positive":
            DATA_BASE[PID]["postive"]["value"] = 1
            DATA_BASE[PID]["postive"]["date"].append(f[0])
            break

    if DATA_BASE[PID]["postive"]["value"] == 0:
        continue

    for f in feature_dic["adm_date"]:
        if A_later_than_B(f[0], DATA_BASE[PID]["postive"]["date"][0], relaxation=240):
            DATA_BASE[PID]["adm"]["value"] = 1
            DATA_BASE[PID]["adm"]["date"].append(f[0])

    if DATA_BASE[PID]["adm"]["value"] != 0:
        for f in feature_dic["icu_date"]:
            if A_later_than_B(
                f[1], DATA_BASE[PID]["postive"]["date"][0], relaxation=240
            ):
                DATA_BASE[PID]["icu"]["value"] = 1
                DATA_BASE[PID]["icu"]["date"].append(f[1])

        for f in feature_dic["intubation_date"]:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], relaxation=240
            ):
                DATA_BASE[PID]["intu"]["value"] = 1
                DATA_BASE[PID]["intu"]["date"].append(f[0])

    for f in feature_dic["death_date"]:
        if A_later_than_B(f[0], DATA_BASE[PID]["postive"]["date"][0], relaxation=240):
            DATA_BASE[PID]["death"]["value"] = 1
            DATA_BASE[PID]["death"]["date"].append(f[0])

    # ------------------ Features - vitals

    for f in feature_dic["BMI (Calculated)"]:
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["bmi"]["value"].append(f[1])
                DATA_BASE[PID]["bmi"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["bmi"]["value"].append(f[1])
                DATA_BASE[PID]["bmi"]["date"].append(f[0])

    for f in feature_dic["BP"]:
        if f[1] == "":
            continue
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["s_bp"]["value"].append(f[1].split("/")[0])
                DATA_BASE[PID]["d_bp"]["value"].append(f[1].split("/")[1])
                DATA_BASE[PID]["s_bp"]["date"].append(f[0])
                DATA_BASE[PID]["d_bp"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["s_bp"]["value"].append(f[1].split("/")[0])
                DATA_BASE[PID]["d_bp"]["value"].append(f[1].split("/")[1])
                DATA_BASE[PID]["s_bp"]["date"].append(f[0])
                DATA_BASE[PID]["d_bp"]["date"].append(f[0])

    for f in feature_dic["Pulse"]:
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["pulse"]["value"].append(f[1])
                DATA_BASE[PID]["pulse"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["pulse"]["value"].append(f[1])
                DATA_BASE[PID]["pulse"]["date"].append(f[0])

    for f in feature_dic["Resp"]:
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["resp"]["value"].append(f[1])
                DATA_BASE[PID]["resp"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["resp"]["value"].append(f[1])
                DATA_BASE[PID]["resp"]["date"].append(f[0])

    for f in feature_dic["SpO2"]:
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["spo2"]["value"].append(f[1])
                DATA_BASE[PID]["spo2"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["spo2"]["value"].append(f[1])
                DATA_BASE[PID]["spo2"]["date"].append(f[0])

    for f in feature_dic["Temp"]:
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                DATA_BASE[PID]["temp"]["value"].append(f[1])
                DATA_BASE[PID]["temp"]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                DATA_BASE[PID]["temp"]["value"].append(f[1])
                DATA_BASE[PID]["temp"]["date"].append(f[0])

    # ------------------ Features - radiology

    for f in feature_dic["radiology findings"]:
        if A_later_than_B(
            f[0], DATA_BASE[PID]["postive"]["date"][0], 96, relaxation=96
        ):
            for r in f[1:]:
                if r in ["opacity", "opacities"]:
                    DATA_BASE[PID]["Opacity"]["value"] = 1
                    DATA_BASE[PID]["Opacity"]["date"].append(f[0])
                if r in ["atelectasis"]:
                    DATA_BASE[PID]["Atelectasis"]["value"] = 1
                    DATA_BASE[PID]["Atelectasis"]["date"].append(f[0])
                if r in ["consolidation"]:
                    DATA_BASE[PID]["Consolidation"]["value"] = 1
                    DATA_BASE[PID]["Consolidation"]["date"].append(f[0])
                if r in ["pleural effusion"]:
                    DATA_BASE[PID]["Pleural Effusion"]["value"] = 1
                    DATA_BASE[PID]["Pleural Effusion"]["date"].append(f[0])
                if r in ["pneumothorax"]:
                    DATA_BASE[PID]["Pneumothorax"]["value"] = 1
                    DATA_BASE[PID]["Pneumothorax"]["date"].append(f[0])
                if r in ["nodule"]:
                    DATA_BASE[PID]["Nodule"]["value"] = 1
                    DATA_BASE[PID]["Nodule"]["date"].append(f[0])

    # ------------------ Features - symptom

    for f in feature_dic["other disease or symptoms"]:
        sym_dic = {}
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                sym_dic = check_sym(f[1:])
            for k in sym_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                sym_dic = check_sym(f[1:])
            for k in sym_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])

    for f in feature_dic["symptom"]:
        sym_dic = {}
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                DATA_BASE[PID]["postive"]["date"][0],
                DATA_BASE[PID]["adm"]["date"][0],
                relaxation=48,
            ):
                sym_dic = check_sym(f[1:])
            for k in sym_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])
        else:
            if A_later_than_B(
                f[0], DATA_BASE[PID]["postive"]["date"][0], 48, relaxation=48
            ):
                sym_dic = check_sym(f[1:])
            for k in sym_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])

    # ------------------ Features - pmh

    for f in feature_dic["other disease or symptoms"]:
        pmh_dic = {}
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])
        else:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])

    for f in feature_dic["immune"]:
        pmh_dic = {}
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])
        else:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])

    for f in feature_dic["past medical history"]:
        pmh_dic = {}
        if DATA_BASE[PID]["adm"]["value"] == 1:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])
        else:
            if A_later_than_B_before_C(
                f[0],
                "01/01/1901 00:00:00",
                DATA_BASE[PID]["postive"]["date"][0],
                relaxation=48,
            ):
                pmh_dic = check_pmh(f[1:])
            for k in pmh_dic:
                DATA_BASE[PID][k]["value"] = 1
                DATA_BASE[PID][k]["date"].append(f[0])


print(DATA_BASE["1"])

out = open("label_vitals_radiology_pmh_sym.csv", "a", newline="", encoding="utf-8")
csv_write = csv.writer(out, dialect="excel")


head = ["PID"]
head.extend([k for k in empty_variables_dic])
csv_write.writerow(head)


for PID in DATA_BASE:

    if DATA_BASE[PID]["postive"]["value"] == 0:
        continue

    out_line = [PID]

    for feature in head[1:]:
        if feature in ["bmi", "s_bp", "d_bp", "pulse", "resp", "spo2", "temp"]:
            try:
                out_line.append(DATA_BASE[PID][feature]["value"][0])
            except IndexError:
                out_line.append("")
        else:
            out_line.append(DATA_BASE[PID][feature]["value"])

    csv_write.writerow(out_line)


out.close()
