# BMC_COVID

Source code for paper:

[Development and Validation of Predictive Models for COVID-19 Outcomes in a Safety-net Hospital Population](https://pubmed.ncbi.nlm.nih.gov/35441692/)

Due to a restrictive data use agreement and HIPAA rules, we do not provide the data or part of the code for data pre-processing.

The LSTM-transformer model was built under Tensorflow 1.15.0.
